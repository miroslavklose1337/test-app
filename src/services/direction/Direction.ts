import { dispatch } from "../../storage/store";
import sliceDirection from "../../storage/slices/direction";
import IApiService from "../api/IApiService";
import { DirectionCode, DirectionCodeMap } from "../../typings/direction";

export default class DirectionService {
  private apiService: IApiService;
  private storageActions = sliceDirection.actions;

  constructor(apiService: IApiService) {
    this.apiService = apiService;
  }

  public getDirections = async () => {
    try {
      dispatch(this.storageActions.startDirectionsLoading());
      const dtoRes = await this.apiService.getDirections();
      const directions = dtoRes;
      dispatch(this.storageActions.getDirectionsSuccess(directions));
    } catch (error) {
      dispatch(this.storageActions.hasError(error));
    }
  };

  public getDirectionsExchangeMap = async () => {
    try {
      dispatch(this.storageActions.startDirectionsExchangeMapLoading());
      const dtoRes = await this.apiService.getDirectionsExchangeMap();
      const map: Partial<{ [Key in DirectionCode]: DirectionCodeMap }> = {};

      dtoRes.forEach((data) => {
        const allowedExchangeDirectionsMap: DirectionCodeMap = {};
        data.to.forEach((d) => {
          allowedExchangeDirectionsMap[d.code] = d;
        });
        map[data.from.code] = allowedExchangeDirectionsMap;
      });

      dispatch(this.storageActions.getDirectionsExchangeMapSuccess(map));
    } catch (error) {
      dispatch(this.storageActions.hasError(error));
    }
  };
}
