import { DirectionsDTOResponse, DirectionsExchangeMapDTOResponse } from "../../typings/dto/direction";

export default interface IApiService {
  getDirections: () => Promise<DirectionsDTOResponse>;
  getDirectionsExchangeMap: () => Promise<DirectionsExchangeMapDTOResponse>;
}
