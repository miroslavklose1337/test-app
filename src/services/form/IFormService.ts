import { Category } from "../../typings/category";
import { DirectionCode } from "../../typings/direction";

export default interface IFormService {
  changeCategory: (category: Category) => void;
  changeDirection: (directionCode: DirectionCode) => void;
}
