import store from "../../storage/store";
import sliceFrom from "../../storage/slices/fromForm";
import { Category, categoryToDirectionsMap } from "../../typings/category";
import IFormService from "./IFormService";
import { DirectionCode } from "../../typings/direction";

export default class FormServiceFromImpl implements IFormService {
  private formStorageActions = sliceFrom.actions;
  private toFormService: IFormService;

  constructor(toFormService: IFormService) {
    this.toFormService = toFormService;
  }

  public changeCategory = (category: Category) => {
    const directionCodeList = categoryToDirectionsMap[category];
    const directionCodeSelected = directionCodeList[0] || null;
    store.dispatch(this.formStorageActions.setState({ category, directionCodeSelected, directionCodeList }));

    // Пересчитываем to form, т.к. она зависит от данных from form, а они изменились
    this.toFormService.changeCategory(Category.ALL);
  };

  public changeDirection = (directionCode: DirectionCode) => {
    store.dispatch(this.formStorageActions.setDirectionCodeSelected(directionCode));

    // Пересчитываем to form, т.к. она зависит от данных from form, а они изменились
    this.toFormService.changeCategory(Category.ALL);
  };
}
