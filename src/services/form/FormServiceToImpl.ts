import store from "../../storage/store";
import sliceTo from "../../storage/slices/toForm";
import { Category, categoryToDirectionsMap } from "../../typings/category";
import IFormService from "./IFormService";
import { DirectionCode } from "../../typings/direction";

export default class FormServiceToImpl implements IFormService {
  private formStorageActions = sliceTo.actions;

  public changeCategory = (category: Category) => {
    const { fromForm } = store.getState();
    const directionCodeList = this.getAllowedDirections(category, fromForm.directionCodeSelected);
    const directionCodeSelected = directionCodeList[0] || null;
    store.dispatch(this.formStorageActions.setState({ category, directionCodeSelected, directionCodeList }));
  };

  public changeDirection = (directionCode: DirectionCode) => {
    store.dispatch(this.formStorageActions.setDirectionCodeSelected(directionCode));
  };

  /** Получаем совпавшие направления в разрешенных для обмена и разрешенных в категории */
  private getAllowedDirections = (toFormCategory: Category, fromFormDirectionCodeSelected: DirectionCode | null): DirectionCode[] => {
    const { direction } = store.getState();
    const allowedCategoryDirectionList = categoryToDirectionsMap[toFormCategory];
    const allowedExchangeDirections = (fromFormDirectionCodeSelected && direction.directionsExchangeMap[fromFormDirectionCodeSelected]) || {};

    return allowedCategoryDirectionList.reduce((acc, catDirection) => {
      if (allowedExchangeDirections[catDirection]) acc.push(catDirection);
      return acc;
    }, [] as DirectionCode[]);
  };
}
