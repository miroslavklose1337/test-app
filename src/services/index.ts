import FormServiceFromImpl from "./form/FormServiceFromImpl";
import ApiServiceMockImpl from "./api/ApiServiceMockImpl";
import DirectionService from "./direction/Direction";
import FormServiceToImpl from "./form/FormServiceToImpl";

const apiService = new ApiServiceMockImpl();
const directionsService = new DirectionService(apiService);
const formToService = new FormServiceToImpl();
const formFromService = new FormServiceFromImpl(formToService);

const services = {
  api: apiService,
  directions: directionsService,
  formFrom: formFromService,
  formTo: formToService,
};

export default services;
