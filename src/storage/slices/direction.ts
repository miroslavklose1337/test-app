import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Direction, DirectionCode, DirectionCodeMap } from "../../typings/direction";

type State = {
  isDirectionsLoading: boolean;
  isDirectionsExchangeMapLoading: boolean;
  error: null | Error;
  directionList: Direction[];
  /** Для быстрого получения всего Direction по его code */
  directionsCodeMap: DirectionCodeMap;
  /** Для получения для каждого DirectionCode те направления, на которые можно обменять */
  directionsExchangeMap: Partial<{ [Key in DirectionCode]: DirectionCodeMap }>; // Здесь вместо DirectionCodeMap лучше конечно бы использовать Set, но его нельзя хранить в стейте
};

const initialState: State = {
  error: null,
  isDirectionsLoading: false,
  isDirectionsExchangeMapLoading: false,
  directionList: [],
  directionsCodeMap: {},
  directionsExchangeMap: {},
};

const slice = createSlice({
  name: "direction",
  initialState,
  reducers: {
    startDirectionsLoading(state) {
      state.isDirectionsLoading = true;
    },
    startDirectionsExchangeMapLoading(state) {
      state.isDirectionsExchangeMapLoading = true;
    },
    hasError(state, action) {
      state.isDirectionsLoading = false;
      state.isDirectionsExchangeMapLoading = false;
      state.error = action.payload;
    },
    getDirectionsSuccess(state, action: PayloadAction<Direction[]>) {
      state.isDirectionsLoading = false;
      const map: Partial<{ [Key in DirectionCode]: Direction }> = {};
      action.payload.forEach((v) => (map[v.code] = v));
      state.directionList = action.payload;
      state.directionsCodeMap = map;
    },
    getDirectionsExchangeMapSuccess(state, action: PayloadAction<State["directionsExchangeMap"]>) {
      state.isDirectionsExchangeMapLoading = false;
      state.directionsExchangeMap = action.payload;
    },
  },
});

export default slice;
