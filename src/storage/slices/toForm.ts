import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Category } from "../../typings/category";
import { DirectionCode } from "../../typings/direction";
import { initialFormState } from "./fromForm";

const slice = createSlice({
  name: "toForm",
  initialState: initialFormState,
  reducers: {
    hasError(state, action) {
      state.error = action.payload;
    },
    setDirectionCodeSelected(state, action: PayloadAction<DirectionCode>) {
      state.directionCodeSelected = action.payload;
    },
    setState(state, action: PayloadAction<{ category: Category; directionCodeSelected: DirectionCode; directionCodeList: DirectionCode[] }>) {
      state.category = action.payload.category;
      state.directionCodeSelected = action.payload.directionCodeSelected;
      state.directionCodeList = action.payload.directionCodeList;
    },
  },
});

export default slice;
