import { combineReducers } from "redux";
import { TypedUseSelectorHook, useDispatch as useReduxDispatch, useSelector as useReduxSelector } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import directionSlice from "./slices/direction";
import fromFormSlice from "./slices/fromForm";
import toFormSlice from "./slices/toForm";

const rootReducer = combineReducers({
  direction: directionSlice.reducer,
  fromForm: fromFormSlice.reducer,
  toForm: toFormSlice.reducer,
});
const store = configureStore({
  reducer: rootReducer,
});

type RootState = ReturnType<typeof store.getState>;
type AppDispatch = typeof store.dispatch;

const { dispatch } = store;
const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;
const useDispatch = () => useReduxDispatch<AppDispatch>();

export { store, dispatch, useSelector, useDispatch };
export default store;
