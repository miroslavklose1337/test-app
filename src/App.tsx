import FormContainer from "./components/FormContainer/FormContainer";
import { useState, useEffect } from "react";
import services from "./services";
import { useSelector } from "./storage/store";
import Loader from "./components/Loader/Loader";
import { Category } from "./typings/category";

function App() {
  const { isDirectionsLoading, isDirectionsExchangeMapLoading, error } = useSelector((state) => state.direction);
  const [isInitialized, setIsInitialized] = useState(false);

  useEffect(() => {
    setIsInitialized(true);
    const request1 = services.directions.getDirections();
    const request2 = services.directions.getDirectionsExchangeMap();
    Promise.all([request1, request2]).then(() => services.formFrom.changeCategory(Category.ALL));
  }, []);

  if (error) return <p>Произошла ошибка: {error?.message}</p>;
  else if (!isInitialized || isDirectionsLoading || isDirectionsExchangeMapLoading) return <Loader />;
  else return <FormContainer />;
}

export default App;
