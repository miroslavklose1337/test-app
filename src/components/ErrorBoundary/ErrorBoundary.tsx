import { Component } from "react";

type State = {
  error: Error | false;
};

export default class ErrorBoundary extends Component<{ children: React.ReactNode }, State> {
  constructor(props: any) {
    super(props);
    this.state = { error: false };
  }

  static getDerivedStateFromError(error: Error) {
    return { hasError: error };
  }

  render() {
    if (this.state.error) {
      return <p>{this.state.error?.message || "Произошла ошибка"}</p>;
    }

    return this.props.children;
  }
}
