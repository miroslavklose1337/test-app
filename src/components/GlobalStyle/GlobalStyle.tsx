import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  :root {
    --main-font-color: #666;
    font-family: sans-serif;
    font-size: 24px;
  }

  html {
    height: 100%;
  }

  body {
    color: var(--main-font-color);
    padding: 10px;
    width: 100%;
    height: 100%;
  }

  #root {
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;
