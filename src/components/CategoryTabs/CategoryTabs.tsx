import styled from "styled-components";
import React from "react";
import { Category } from "../../typings/category";
import IFormService from "../../services/form/IFormService";

type Props = {
  currentTab: Category;
  formService: IFormService;
};

function CategoryTabs({ currentTab, formService }: Props) {
  return (
    <FlexContainer>
      {Object.values(Category).map((cat) => {
        const className = cat === currentTab ? "tab_active" : "tab_inactive";
        return (
          <CategoryTabButton key={cat} className={className} onClick={() => formService.changeCategory(cat)}>
            {cat}
          </CategoryTabButton>
        );
      })}
    </FlexContainer>
  );
}

export default React.memo(CategoryTabs, (prevProps, nextProps) => prevProps.currentTab === nextProps.currentTab);

const FlexContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  margin-top: 0.5em;
  & > button {
    margin-right: 5px;
  }
`;

const CategoryTabButton = styled.button`
  font-size: 0.5em;
  border-radius: 5px;
  border: 1px solid transparent;
  background-color: transparent;
  color: var(--main-font-color);
  padding: 5px 10px;
  cursor: pointer;
  &:hover {
    border-color: Coral;
  }
  &.tab_active {
    background-color: Coral;
    color: white;
  }
`;
