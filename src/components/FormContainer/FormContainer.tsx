import styled from "styled-components";
import services from "../../services";
import { useSelector } from "../../storage/store";
import CategoryTabs from "../CategoryTabs/CategoryTabs";
import CurrencyInput from "../CurrencyInput/CurrencyInput";
import ErrorBoundary from "../ErrorBoundary/ErrorBoundary";

export default function FormContainer() {
  const fromFormState = useSelector((state) => state.fromForm);
  const toFormState = useSelector((state) => state.toForm);

  return (
    <Container>
      <ErrorBoundary>
        <Title>Отдаёте</Title>
        <CategoryTabs currentTab={fromFormState.category} formService={services.formFrom} />
        <CurrencyInput
          formService={services.formFrom}
          category={fromFormState.category}
          directionCodeSelected={fromFormState.directionCodeSelected}
          directionCodeList={fromFormState.directionCodeList}
        />
      </ErrorBoundary>

      <ErrorBoundary>
        <Title>Получаете</Title>
        <CategoryTabs currentTab={toFormState.category} formService={services.formTo} />
        <CurrencyInput
          formService={services.formTo}
          category={toFormState.category}
          directionCodeSelected={toFormState.directionCodeSelected}
          directionCodeList={toFormState.directionCodeList}
          fromCategory={fromFormState.category}
          fromDirectionCode={fromFormState.directionCodeSelected}
        />
      </ErrorBoundary>
    </Container>
  );
}

const Container = styled.div`
  width: 400px;
  @media (max-width: 1024px) {
    & {
      width: 100%;
    }
  }
`;

const Title = styled.h2`
  font-size: 1.1em;
  color: Coral;
  margin-top: 20px;
`;
