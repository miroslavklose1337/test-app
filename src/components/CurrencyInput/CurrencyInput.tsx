import styled from "styled-components";
import React, { useState } from "react";
import { useSelector } from "../../storage/store";
import { DirectionCode } from "../../typings/direction";
import { Category } from "../../typings/category";
import IFormService from "../../services/form/IFormService";

type Props = {
  formService: IFormService;
  category: Category;
  directionCodeSelected: DirectionCode | null;
  directionCodeList: DirectionCode[];
  /** Дополнительные данные для React.memo toForm, проще сравнить их, чтобы понять что изменился directionCodeList, чем каждый раз ворочать массив */
  fromCategory?: Category;
  /** Дополнительные данные для React.memo toForm, проще сравнить их, чтобы понять что изменился directionCodeList, чем каждый раз ворочать массив */
  fromDirectionCode?: DirectionCode | null;
};

function CurrencyInput({ directionCodeSelected, directionCodeList, formService }: Props) {
  const { directionsCodeMap } = useSelector((state) => state.direction);
  const [inputState, setInputState] = useState(""); // По ТЗ не задействовано

  return (
    <FlexContainer>
      <InputTextStyled type="number" value={inputState} onChange={(e) => setInputState(e.target.value)} />
      <SelectStyled value={directionCodeSelected || ""} onChange={(e) => formService.changeDirection((e.target.value as DirectionCode) || null)}>
        <option value="" hidden></option>
        {directionCodeList.map((dCode) => (
          <option key={dCode} value={dCode}>
            {directionsCodeMap[dCode]?.name}
          </option>
        ))}
      </SelectStyled>
    </FlexContainer>
  );
}

export default React.memo(
  CurrencyInput,
  (prevProps, nextProps) =>
    prevProps.category === nextProps.category &&
    prevProps.directionCodeSelected === nextProps.directionCodeSelected &&
    prevProps.fromDirectionCode === nextProps.fromDirectionCode &&
    prevProps.fromCategory === nextProps.fromCategory
);

const InputTextStyled = styled.input`
  border: 1px solid #999;
  border-right: none;
  color: #999;
  padding: 10px;
  flex: 1;
`;

const SelectStyled = styled.select`
  width: 150px;
  border: 1px solid #999;
`;

const FlexContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 5px;
`;
