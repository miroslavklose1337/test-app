import { Direction } from "../direction";

export type DirectionsDTOResponse = Direction[];

export type DirectionsExchangeMapDTOResponse = Array<{
  from: Direction;
  to: Direction[];
}>;
