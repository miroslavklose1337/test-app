import { DirectionCode } from "./direction";

export enum Category {
  ALL = "Все",
  CRYPTO = "Криптовалюты",
  BANK = "Банки",
  CASH = "Наличные",
}

/**
 * Это сопоставление из условия задания, где оно было прописано явно, поэтому его не нужно вычислять на основе информации с бекенда.
 * Хотя с последнего приходит больше DirectionCode, чем отмечено здесь (наример TRX, CARDUAH и пр.), поэтому такие данные остаются незадействованными
 */
export const categoryToDirectionsMap: Record<Category, DirectionCode[]> = {
  [Category.CRYPTO]: [DirectionCode.BTC, DirectionCode.ETH, DirectionCode.USDTTRC],
  [Category.BANK]: [DirectionCode.ACRUB, DirectionCode.SBERRUB, DirectionCode.TCSBRUB],
  [Category.CASH]: [DirectionCode.CASHUSD, DirectionCode.CASHRUB],
  [Category.ALL]: Object.values(DirectionCode),
};
