export type Direction = {
  code: DirectionCode;
  name: string;
};

export type DirectionCodeMap = Partial<{ [Key in DirectionCode]: Direction }>;

export enum DirectionCode {
  BTC = "BTC",
  ETH = "ETH",
  USDTTRC = "USDTTRC",
  ACRUB = "ACRUB",
  SBERRUB = "SBERRUB",
  TCSBRUB = "TCSBRUB",
  CASHUSD = "CASHUSD",
  CASHRUB = "CASHRUB",
}
